package net.joostory.holybible.dao;

import java.util.ArrayList;
import java.util.List;

import com.crashlytics.android.Crashlytics;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import net.joostory.holybible.model.Bible;

public class BibleDAO extends DAO {

	private static BibleDAO instance = new BibleDAO();
	private BibleDAO() {}
	public static BibleDAO getInstance() {
		return instance;
	}
	
	
	public List<Bible> getTypeList(Context context, String vcode, String type) {
		List<Bible> list = new ArrayList<Bible>();
		SQLiteDatabase db = null;
		try {
			db = getReadableDatabase(context);
		
			Cursor cursor = db.query("bibles", new String[] { "bcode", "type", "name", "chapter_count" },
					"vcode=? and type=?", new String[] { vcode, type }, null, null, "bcode asc");
			while(cursor.moveToNext()) {
				Bible bible = makeBible(vcode, cursor);
				list.add(bible);
			}
			
		} catch(Throwable t) {
			Crashlytics.logException(t);
		} finally {
			disconnect(db);
		}
		return list;
	}
	
	public List<Bible> getList(Context context, String vcode) {
		List<Bible> list = new ArrayList<Bible>();
		SQLiteDatabase db = null;
		try {
			db = getReadableDatabase(context);
			String query = "select bcode, type, name, chapter_count from bibles where vcode='" + vcode + "'";
			Cursor cursor = db.rawQuery(query, null);
			while(cursor.moveToNext()) {
				Bible bible = makeBible(vcode, cursor);
				list.add(bible);
			}
		} catch(Throwable t) {
			Crashlytics.logException(t);
		} finally {
			disconnect(db);
		}
		return list;
	}
	
	private Bible makeBible(String vcode, Cursor cursor) {
		Bible bible = new Bible();
		bible.setVcode(vcode);
		bible.setCode(cursor.getInt(0));
		bible.setType(cursor.getString(1));
		bible.setName(cursor.getString(2));
		bible.setChapterCount(cursor.getInt(3));
		return bible;
	}
	
	public Bible get(Context context, String vcode, String bcode) {
		Bible bible = new Bible();
		SQLiteDatabase db = null;
		try {
			db = getReadableDatabase(context);
			String query = "select bcode, type, name, chapter_count from bibles where vcode='" + vcode + "' and bcode='" + bcode + "'";
			Cursor cursor = db.rawQuery(query, null);
			if(cursor.moveToNext()) {
				bible = makeBible(vcode, cursor);
			}
		} catch(Throwable t) {
			Crashlytics.logException(t);
		} finally {
			disconnect(db);
		}
		return bible;
	}
	
	public Bible get(Context context, String vcode, int bcode) {
		Bible bible = new Bible();
		SQLiteDatabase db = null;
		try {
			db = getReadableDatabase(context);
			Cursor cursor = db.query("bibles", new String[] {
				"bcode", "type", "name", "chapter_count"
			}, "vcode=? and bcode=?", new String[] {
				vcode,
				Integer.toString(bcode)
			}, null, null, null);
			
			if(cursor.moveToNext()) {
				bible = makeBible(vcode, cursor);
			}
		} catch(Throwable t) {
			Crashlytics.logException(t);
		} finally {
			disconnect(db);
		}
		
		return bible;
	}
	
	public int getCount(Context context, String vcode) {
		int count = 0;
		SQLiteDatabase db = null;
		try {
			db = getReadableDatabase(context);
			Cursor cursor = db.query("bibles", new String[] {
				"count(vcode)"
			}, "vcode=?", new String[] {
				vcode
			}, null, null, null);
			
			if(cursor.moveToNext()) {
				count = cursor.getInt(0);
			}
		} catch(Throwable t) {
			Crashlytics.logException(t);
		} finally {
			disconnect(db);
		}
		
		return count;
	}
}
