package net.joostory.holybible.dao;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

public abstract class DAO {
	
	public void disconnect(SQLiteDatabase db) {
		if (db != null) {
			db.close();
		}
	}
	
	public SQLiteDatabase getReadableDatabase(Context context) {
		
		DatabaseHelper helper = new DatabaseHelper(context);
		if (!helper.checkDataBase()) {
			helper.createDataBase();
		}
		return helper.getReadableDatabase();
	}
}
