package net.joostory.holybible.dao;

import java.util.ArrayList;
import java.util.List;

import com.crashlytics.android.Crashlytics;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import net.joostory.holybible.model.Version;

public class VersionDAO extends DAO {

	private static VersionDAO instance = new VersionDAO();
	private VersionDAO() {}
	public static VersionDAO getInstance() {
		return instance;
	}
	
	public List<Version> getList(Context context) {
		List<Version> list = new ArrayList<Version>();
		SQLiteDatabase db = null;
		try {
			db = getReadableDatabase(context);
			String query = "select vcode, name from versions;";
			Cursor cursor = db.rawQuery(query, null);
			while(cursor.moveToNext()) {
				Version version = new Version();
				version.setCode(cursor.getString(0));
				version.setName(cursor.getString(1));
				list.add(version);
			}
		} catch(Throwable t) {
			Crashlytics.logException(t);
		} finally {
			disconnect(db);
		}
		return list;
	}
	public Version get(Context context, String vcode) {
		Version version = new Version();
		SQLiteDatabase db = null;
		try {
			db = getReadableDatabase(context);
			String query = "select vcode, name from versions where vcode='" + vcode + "';";
			Cursor cursor = db.rawQuery(query, null);
			if (cursor.moveToNext()) {
				version.setCode(cursor.getString(0));
				version.setName(cursor.getString(1));
			}
		} catch(Throwable t) {
			Crashlytics.logException(t);
		} finally {
			disconnect(db);
		}
		return version;
	}
}
