package net.joostory.holybible.dao;

import java.util.ArrayList;
import java.util.List;

import com.crashlytics.android.Crashlytics;

import net.joostory.holybible.model.Verse;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public class VerseDAO extends DAO {
	
	private static final String[] COLUMNS = new String[] { "vcode", "bcode", "cnum", "vnum", "content" };
	private static final String[] SEARCH_COLUMNS = new String[] { "v.vcode", "v.bcode", "b.name", "v.cnum", "v.vnum", "v.content" };
	private static VerseDAO instance = new VerseDAO();
	private VerseDAO() {}
	public static VerseDAO getInstance() {
		return instance;
	}
	
	public List<Verse> getList(Context context, String vcode, int bcode, int cnum) {
		List<Verse> list = new ArrayList<Verse>();
		SQLiteDatabase db = null;
		try {
			db  = getReadableDatabase(context);
			Cursor cursor = db.query("verses", COLUMNS, "vcode=? and bcode=? and cnum=?", new String[] {
					vcode, Integer.toString(bcode), Integer.toString(cnum)
			}, null, null, "cnum asc");
			while(cursor.moveToNext()) {
				Verse verse = makeVerseWithCursor(cursor);
				list.add(verse);
			}
		} catch(Throwable t) {
			Crashlytics.logException(t);
		} finally {
			disconnect(db);
		}
		return list;
	}
	
	private Verse makeVerseWithCursor(Cursor cursor) {
		Verse verse = new Verse();
		int i = 0;
		verse.setVcode(cursor.getString(i++));
		verse.setBcode(cursor.getString(i++));
		verse.setCnum(cursor.getInt(i++));
		verse.setVnum(cursor.getInt(i++));
		verse.setContent(cursor.getString(i++));
		return verse;
	}
	
	public int getCount(Context context, String vcode, int bcode, int cnum) {
		int count=0;
		SQLiteDatabase db = null;
		try {
			db  = getReadableDatabase(context);
			Cursor cursor = db.query("verses", new String[] {
					"count(cnum)"
			}, "vcode=? and bcode=? and cnum=?", new String[] {
					vcode, Integer.toString(bcode), Integer.toString(cnum)
			}, null, null, "cnum asc");
			if(cursor.moveToNext()) {
				count = cursor.getInt(0);
			}
		} catch(Throwable t) {
			Crashlytics.logException(t);
		} finally {
			disconnect(db);
		}
		return count;
	}
	
	public List<Verse> search(Context context, String vcode, String searchString) {
		List<Verse> list = new ArrayList<Verse>();
		SQLiteDatabase db = null;
		try {
			db  = getReadableDatabase(context);
			Cursor cursor = db.query("verses v, bibles b", SEARCH_COLUMNS,
					"v.vcode=b.vcode and v.bcode=b.bcode and v.vcode=? and v.content like ?", new String[] {
						vcode, "%" + searchString + "%"
					}, null, null, "cnum asc");
			while(cursor.moveToNext()) {
				Verse verse = makeSearchVerseWithCursor(cursor);
				list.add(verse);
			}
		} catch(Throwable t) {
			Crashlytics.logException(t);
		} finally {
			disconnect(db);
		}
		return list;
	}
	
	private Verse makeSearchVerseWithCursor(Cursor cursor) {
		Verse verse = new Verse();
		int i = 0;
		verse.setVcode(cursor.getString(i++));
		verse.setBcode(cursor.getString(i++));
		verse.setBname(cursor.getString(i++));
		verse.setCnum(cursor.getInt(i++));
		verse.setVnum(cursor.getInt(i++));
		verse.setContent(cursor.getString(i++));
		return verse;
	}
	
}
