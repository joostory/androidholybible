package net.joostory.holybible.dao;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import com.crashlytics.android.Crashlytics;

import net.joostory.holybible.exception.MigrateFailException;
import net.joostory.holybible.util.SqlUtil;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DatabaseHelper extends SQLiteOpenHelper {
	
    private static String DB_NAME = "holybible.db";
 
    private final Context context;
	private static final int VERSION = 4;
	
	public DatabaseHelper(Context context) {
		super(context, DB_NAME, null, VERSION);
		this.context = context;
	}

	@Override
	public void onCreate(SQLiteDatabase db) { }

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		if (oldVersion < 2 && newVersion >= 2) {
			migrate(db, "migrate_1to2.sql");
		}
		if (oldVersion < 3 && newVersion >= 3) {
			migrate(db, "migrate_2to3.sql");
		}
		if (oldVersion < 4 && newVersion >= 4) {
			migrate(db, "migrate_3to4.sql");
		}
	}

	private void migrate(SQLiteDatabase db, String filename) {
		try {
			SqlUtil.executeSql(db, context.getAssets().open(filename));
		} catch (IOException e) {
			throw new MigrateFailException(e);
		}
	}

	public void createDataBase() {
    	getReadableDatabase();
    	try {
    		copyDataBase();
    	} catch(Throwable t) {
    		Crashlytics.logException(t);
    	}
    }
    
    public boolean checkDataBase(){
    	SQLiteDatabase checkDB = null;
    	try {
    		checkDB = SQLiteDatabase.openDatabase(context.getDatabasePath(DB_NAME).getAbsolutePath(), null, SQLiteDatabase.OPEN_READONLY);
    		return checkDB != null;
    	} catch (Throwable t){
    		//database does't exist yet.
    	} finally {
    		if(checkDB != null){
        		checkDB.close();
        	}	
    	}
    	return false;
    }
    
    private void copyDataBase() throws IOException{
 
    	InputStream myInput = context.getAssets().open(DB_NAME);
    	OutputStream myOutput = new FileOutputStream(context.getDatabasePath(DB_NAME));

    	byte[] buffer = new byte[1024];
    	int length;
    	while ((length = myInput.read(buffer))>0){
    		myOutput.write(buffer, 0, length);
    	}
 
    	myOutput.flush();
    	myOutput.close();
    	myInput.close();
    }
    
}
