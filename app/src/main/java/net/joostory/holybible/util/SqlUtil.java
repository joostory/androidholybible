package net.joostory.holybible.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import android.database.sqlite.SQLiteDatabase;

public class SqlUtil {

	public static void executeSql(SQLiteDatabase db, InputStream is) throws IOException {
		BufferedReader in = new BufferedReader(new InputStreamReader(is));
		String line = null;
		while((line = in.readLine()) != null) {
			db.execSQL(line);
		}
		in.close();
	}

}
