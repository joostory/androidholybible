package net.joostory.holybible.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

public class PreferenceUtils {

	private static SharedPreferences getPreference(Context context) {
		return context.getSharedPreferences("holybible", Context.MODE_PRIVATE);
	}

	public static int getInt(Context context, String key, int defValue) {
		return getPreference(context).getInt(key, defValue);
	}
	
	public static void putInt(Context context, String key, int value) {
		Editor edit = getPreference(context).edit();
		edit.putInt(key, value);
		edit.commit();
	}
	
	public static float getFloat(Context context, String key, float defValue) {
		return getPreference(context).getFloat(key, defValue);
	}
	
	public static void putFloat(Context context, String key, float value) {
		Editor edit = getPreference(context).edit();
		edit.putFloat(key, value);
		edit.commit();
	}
	
}
