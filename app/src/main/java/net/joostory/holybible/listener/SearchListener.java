package net.joostory.holybible.listener;

import android.widget.SearchView.OnQueryTextListener;

public abstract class SearchListener implements OnQueryTextListener {

	public SearchListener() {
	}

	@Override
	public boolean onQueryTextChange(String text) {
		return false;
	}

	@Override
	public boolean onQueryTextSubmit(String text) {
		startSearch(text);
		return false;
	}
	
	protected abstract void startSearch(String text);

}
