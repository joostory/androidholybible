package net.joostory.holybible.exception;

public class MigrateFailException extends RuntimeException {

	private static final long serialVersionUID = 7597420742372085656L;

	public MigrateFailException(Exception e) {
		super(e);
	}
}
