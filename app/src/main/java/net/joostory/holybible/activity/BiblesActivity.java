package net.joostory.holybible.activity;

import com.crashlytics.android.Crashlytics;
import java.util.Locale;

import net.joostory.holybible.R;
import net.joostory.holybible.adapter.BibleAdapter;
import net.joostory.holybible.model.Bible;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ExpandableListView;
import android.widget.ExpandableListView.OnChildClickListener;
import android.widget.ExpandableListView.OnGroupClickListener;

public class BiblesActivity extends ActionBarBaseActivity {

	private ExpandableListView mView;
	private BibleAdapter mAdapter;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
        Crashlytics.start(this);
		setContentView(R.layout.bible_list);
        
		if (Locale.getDefault().getLanguage().equals(Locale.KOREAN.getLanguage())) {
			vcode = "GAE";
		} else { 
			vcode = "NIV";
		}
        init();
	}
	
	protected void init() {
		mAdapter = new BibleAdapter(this, vcode);
		
		mView = (ExpandableListView) findViewById(R.id.list);
		mView.setAdapter(mAdapter);
        mView.setOnGroupClickListener(new OnGroupClickListener() {
			@Override
			public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {
				return true;
			}
		});
        
        mView.setOnChildClickListener(new OnChildClickListener() {
			@Override
			public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
				Bible bible = (Bible) mAdapter.getChild(groupPosition, childPosition);
				Intent intent = new Intent(BiblesActivity.this, ChaptersActivity.class);
				intent.putExtra("vcode", bible.getVcode());
				intent.putExtra("bcode", bible.getCode());
				startActivity(intent);
				
				return true;
			}
		});
	}

}
