package net.joostory.holybible.activity;

import java.util.List;

import net.joostory.holybible.R;
import net.joostory.holybible.dao.VerseDAO;
import net.joostory.holybible.model.Verse;
import android.content.Intent;
import android.os.Bundle;
import android.webkit.WebView;

public class SearchActivity extends ActionBarBaseActivity {

	private String text;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.search_list);
		
        Intent intent = getIntent();
        Bundle extras = intent.getExtras();
        vcode = extras.getString("vcode");
        text = extras.getString("text");
        
        init();
	}
	
	@Override
	protected void init() {
		WebView view = (WebView) findViewById(R.id.content);
		view.loadData(makeSearchData(), "text/html;charset=utf-8", "utf-8");
		
		setTitle("Search : " + text);
	}
	
	private String makeSearchData() {
		StringBuffer buf = new StringBuffer();
		
		buf.append("<!doctype html><html><head><meta charset='utf-8'><meta name='viewport' content='width=device-width; initial-scale=1.0; '/><style>")
			.append("dl { margin:10px; }")
			.append("dt { font-size:12px; font-weight:bold; }")
			.append("dd { margin:0 0 10px; font-size:14px; padding:5px 0 }")
			.append("dd b { color:#f00 }")
			.append("</style></head><body><dl>");
		
		List<Verse> list = VerseDAO.getInstance().search(getApplicationContext(), vcode, text);
		for (Verse v : list) {
			String title = v.getBname() + " " + v.getCnum() + ":" + v.getVnum();
			String content = v.getContent().replaceAll(text, "<b>" + text + "</b>");
			buf.append("<dt>").append(title).append("</dt>")
				.append("<dd>").append(content).append("</dd>");
		}
		
		buf.append("</dl></body></html>");
		
		return buf.toString();
	}

}
