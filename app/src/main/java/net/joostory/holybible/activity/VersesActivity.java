package net.joostory.holybible.activity;

import net.joostory.holybible.R;
import net.joostory.holybible.adapter.VersesPagerAdapter;
import net.joostory.holybible.dao.BibleDAO;
import net.joostory.holybible.model.Bible;
import net.joostory.holybible.util.PreferenceUtils;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.SearchView;
import android.widget.TextView;

public class VersesActivity extends ActionBarBaseActivity implements OnPageChangeListener, View.OnClickListener {

    public static final int TEXT_SIZE_MIN = 8;
    public static final int TEXT_SIZE_MAX = 36;
    public static final int TEXT_SIZE_DEFAULT = 14;
    public static final int TEXT_SIZE_INTERVAL = 2;

	private Bible bible;
	
	private int bcode;
	private int cnum;

    private TextView mTextSize;
	private ViewPager mPager;
    private VersesPagerAdapter mPagerAdapter;


    @Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_verses);
		
        Intent intent = getIntent();
        Bundle extras = intent.getExtras();
        vcode = extras.getString("vcode");
        bcode = extras.getInt("bcode");
        cnum = extras.getInt("cnum");
        
        init();
	}
	
	protected void init() {
		bible = BibleDAO.getInstance().get(this, vcode, bcode);

		mPager = (ViewPager) findViewById(R.id.list);
		mPager.refreshDrawableState();
		mPager.setOnPageChangeListener(this);
		
		mPagerAdapter = new VersesPagerAdapter(getSupportFragmentManager(), bible);
		mPager.setAdapter(mPagerAdapter);
		mPager.setCurrentItem(cnum - 1);
		
        setBibleTitle();

        mTextSize = (TextView) findViewById(R.id.text_size);
        int textSize = PreferenceUtils.getInt(this, "textSize", 14);
        mTextSize.setText(String.valueOf(textSize));
        mTextSize.setTextSize(textSize);
        findViewById(R.id.text_bigger).setOnClickListener(this);
        findViewById(R.id.text_smaller).setOnClickListener(this);
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.verses_menu, menu);
		mSearchView = (SearchView) menu.findItem(R.id.menu_search).getActionView();
		mSearchView.setOnQueryTextListener(mSearchListener);
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
	    switch (item.getItemId()) {
	    case R.id.text_bigger:
	    	increaseTextSize();
	    	return true;
        case R.id.text_default:
            defaultTextSize();
            return true;
	    case R.id.text_smaller:
	    	decreaseTextSize();
	    	return true;
        default:
            return super.onOptionsItemSelected(item);
	    }
	}
	
	private void decreaseTextSize() {
        int textSize = PreferenceUtils.getInt(this, "textSize", TEXT_SIZE_DEFAULT);
        if (textSize > TEXT_SIZE_MIN) {
            textSize -= TEXT_SIZE_INTERVAL;
        }
        changeTextSize(textSize);
	}

    private void defaultTextSize() {
        changeTextSize(TEXT_SIZE_DEFAULT);
    }

	private void increaseTextSize() {
        int textSize = PreferenceUtils.getInt(this, "textSize", TEXT_SIZE_DEFAULT);
        if (textSize < TEXT_SIZE_MAX) {
            textSize += TEXT_SIZE_INTERVAL;
        }
        changeTextSize(textSize);
	}

    private void changeTextSize(int textSize) {
        PreferenceUtils.putInt(this, "textSize", textSize);

        mTextSize.setText(String.valueOf(textSize));
        mTextSize.setTextSize(textSize);

        Intent intent = new Intent();
        intent.setAction("net.joostory.holybible.action.TEXT_SIZE");
        intent.putExtra("textSize", textSize);
        sendBroadcast(intent);
    }

	private void setBibleTitle() {
		String title = bible.getName() + " " + cnum;
		setTitle(title);
	}
	
	@Override
	public void onPageScrollStateChanged(int arg0) {
	}

	@Override
	public void onPageScrolled(int arg0, float arg1, int arg2) {
	}

	@Override
	public void onPageSelected(int pointer) {
		cnum = pointer + 1;
		setBibleTitle();
	}

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
        case R.id.text_bigger:
            increaseTextSize();
            break;
        case R.id.text_smaller:
            decreaseTextSize();
            break;
        }
    }
}
