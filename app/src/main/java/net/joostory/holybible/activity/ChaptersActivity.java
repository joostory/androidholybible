package net.joostory.holybible.activity;

import net.joostory.holybible.R;
import net.joostory.holybible.adapter.ChapterPagerAdapter;
import net.joostory.holybible.dao.BibleDAO;
import net.joostory.holybible.model.Bible;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;

public class ChaptersActivity extends ActionBarBaseActivity implements OnPageChangeListener {
	
	private ViewPager mPager;
	private Bible bible;

	private int bcode;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.pagelist);
		
		Intent intent = getIntent();
		Bundle bundle = intent.getExtras();
		vcode = bundle.getString("vcode");
		bcode = bundle.getInt("bcode");
		
		init();
	}
	
	protected void init() {
		bible = BibleDAO.getInstance().get(this, vcode, bcode);
		
		int bibleCount = BibleDAO.getInstance().getCount(getApplicationContext(), vcode);
		
		mPager = (ViewPager) findViewById(R.id.list);
		mPager.setAdapter(new ChapterPagerAdapter(getSupportFragmentManager(), vcode, bibleCount));
		mPager.setOnPageChangeListener(this);
		mPager.setCurrentItem(bcode - 1, true);

		setBibleTitle();
	}
	
	private void setBibleTitle() {
		String title = bible.getName();
		setTitle(title);
	}

	
	@Override
	public void onPageScrollStateChanged(int arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onPageScrolled(int arg0, float arg1, int arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onPageSelected(int position) {
		bcode = position + 1;
		bible = BibleDAO.getInstance().get(getApplicationContext(), vcode, bcode);
		setBibleTitle();
	}
	
}
