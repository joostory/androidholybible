package net.joostory.holybible.activity;

import com.google.analytics.tracking.android.EasyTracker;

import net.joostory.holybible.BuildConfig;
import net.joostory.holybible.R;
import net.joostory.holybible.listener.SearchListener;

import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.NavUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.SearchView;
import android.widget.TextView;

public abstract class ActionBarBaseActivity extends FragmentActivity {

	protected String vcode;
	protected SearchView mSearchView;
	protected SearchListener mSearchListener;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mSearchListener = new SearchListener() {

			@Override
			protected void startSearch(String text) {
				Intent intent = new Intent(ActionBarBaseActivity.this, SearchActivity.class);
				intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				intent.putExtra("vcode", vcode);
				intent.putExtra("text", text);
				startActivity(intent);
			}
			
		};
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.read_menu, menu);
		mSearchView = (SearchView) menu.findItem(R.id.menu_search).getActionView();
		mSearchView.setOnQueryTextListener(mSearchListener);
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
	    switch (item.getItemId()) {
		    case android.R.id.home:
				NavUtils.navigateUpFromSameTask(this);
				return true;
	        case R.id.in_korean:
	        	vcode = "GAE";
	        	init();
	            return true;
	        case R.id.in_english:
	        	vcode = "NIV";
	        	init();
	            return true;
            case R.id.menu_about:
                showAbout();
                return true;
	        default:
	            return super.onOptionsItemSelected(item);
	    }
	}

	abstract protected void init();
	
	@Override
	public void onStart() {
		super.onStart();
        if (!BuildConfig.DEBUG) {
            EasyTracker.getInstance(this).activityStart(this);
        }
	}

	@Override
	public void onStop() {
		super.onStop();
        if (!BuildConfig.DEBUG) {
            EasyTracker.getInstance(this).activityStop(this);
        }
	}

    protected void showAbout() {
        Dialog dialog = new Dialog(this, android.R.style.Theme_Holo_Light_Dialog_NoActionBar_MinWidth);
        dialog.setContentView(R.layout.activity_about);

        TextView tv = (TextView) dialog.findViewById(R.id.app_info);
        try {
            PackageInfo p = getPackageManager().getPackageInfo(getPackageName(), 0);
            tv.setText(String.format("%s (%s)\n%s", p.versionName, p.versionCode, p.packageName));
        } catch (PackageManager.NameNotFoundException e) {
            // do nothing
        }
        dialog.show();
    }
}
