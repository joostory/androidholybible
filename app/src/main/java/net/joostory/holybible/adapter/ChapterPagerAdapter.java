package net.joostory.holybible.adapter;

import net.joostory.holybible.fragment.ChapterFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

public class ChapterPagerAdapter extends FragmentStatePagerAdapter {

	private String vcode;
	private int count;

	public ChapterPagerAdapter(FragmentManager fm, String vcode, int count) {
		super(fm);
		this.vcode = vcode;
		this.count = count;
	}

	@Override
	public int getCount() {
		return count;
	}

	@Override
	public Fragment getItem(int position) {
		return ChapterFragment.newInstance(vcode, position + 1);
	}

}