package net.joostory.holybible.adapter;

import java.util.List;

import net.joostory.holybible.R;
import net.joostory.holybible.dao.BibleDAO;
import net.joostory.holybible.model.Bible;
import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.TextView;

public class BibleAdapter extends BaseExpandableListAdapter {

	private static final int OLDTESTAMENT_GROUP_POSITION = 0;
	private List<Bible> oldList = null;
	private List<Bible> newList = null;
	private LayoutInflater mInflater;
	
	public BibleAdapter(Context context, String vcode) {
		mInflater = LayoutInflater.from(context);
		oldList = BibleDAO.getInstance().getTypeList(context, vcode, "old");
		newList = BibleDAO.getInstance().getTypeList(context, vcode, "new");
	}

	@Override
	public boolean areAllItemsEnabled() {
		return true;
	}
	
	@Override
	public Object getChild(int groupPosition, int childPosition) {
		if (groupPosition == OLDTESTAMENT_GROUP_POSITION) {
			return oldList.get(childPosition);
		} else {
			return newList.get(childPosition);
		}
	}


	@Override
	public long getChildId(int groupPosition, int childPosition) {
		if (groupPosition == 0) {
			return oldList.get(childPosition).getCode();
		} else {
			return newList.get(childPosition).getCode();
		}
	}


	@Override
	public View getChildView(int groupPosition, int childPosition,
			boolean isLastChild, View convertView, ViewGroup parent) {
		
		if (convertView == null) {
			convertView = mInflater.inflate(R.layout.list_row, null);
		}
		
		Bible bible = (Bible) getChild(groupPosition, childPosition);
		TextView tv = (TextView) convertView.findViewById(R.id.name);
		tv.setText(bible.getName());
		return convertView;
	}


	@Override
	public int getChildrenCount(int groupPosition) {
		if (groupPosition == OLDTESTAMENT_GROUP_POSITION) {
			return oldList.size();
		} else {
			return newList.size();
		}
	}


	@Override
	public Object getGroup(int groupPosition) {
		if (groupPosition == 0) {
			return oldList;
		} else {
			return newList;
		}
	}


	@Override
	public int getGroupCount() {
		return 2;
	}


	@Override
	public long getGroupId(int groupPosition) {
		return groupPosition;
	}


	@Override
	public View getGroupView(int groupPosition, boolean isExpanded,
			View convertView, ViewGroup parent) {
		
		if (convertView == null) {
			convertView = mInflater.inflate(R.layout.bible_list_header, null);
		}
		
		TextView tv = (TextView) convertView.findViewById(R.id.name);
		tv.setTypeface(null, Typeface.BOLD);
		if (groupPosition == OLDTESTAMENT_GROUP_POSITION) {
			tv.setText(R.string.old_testament);
		} else {
			tv.setText(R.string.new_testament);
		}
		
		((ExpandableListView) parent).expandGroup(groupPosition);
		
		return convertView;
	}


	@Override
	public boolean hasStableIds() {
		return false;
	}


	@Override
	public boolean isChildSelectable(int groupPosition, int childPosition) {
		return true;
	}
}
