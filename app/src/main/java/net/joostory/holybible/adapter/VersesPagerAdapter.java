package net.joostory.holybible.adapter;

import net.joostory.holybible.fragment.VersesFragment;
import net.joostory.holybible.model.Bible;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

public class VersesPagerAdapter extends FragmentStatePagerAdapter {

	private Bible bible;

	public VersesPagerAdapter(FragmentManager fm, Bible bible) {
		super(fm);
		this.bible = bible;
	}

	@Override
	public Fragment getItem(int pointer) {
		return VersesFragment.newInstance(bible.getVcode(), bible.getCode(), pointer + 1);
	}
	
	@Override
	public int getCount() {
		return bible.getChapterCount();
	}

}
