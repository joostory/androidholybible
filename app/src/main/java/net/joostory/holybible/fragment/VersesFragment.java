package net.joostory.holybible.fragment;

import java.util.List;

import com.crashlytics.android.Crashlytics;

import net.joostory.holybible.R;
import net.joostory.holybible.activity.VersesActivity;
import net.joostory.holybible.dao.VerseDAO;
import net.joostory.holybible.model.Verse;
import net.joostory.holybible.util.PreferenceUtils;
import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.JavascriptInterface;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;

public class VersesFragment extends Fragment {

	public static Fragment newInstance(String vcode, int bcode, int cnum) {
		VersesFragment fragment = new VersesFragment();
		Bundle bundle = new Bundle();
		bundle.putString("vcode", vcode);
		bundle.putInt("bcode", bcode);
		bundle.putInt("cnum", cnum);
		fragment.setArguments(bundle);
		fragment.setRetainInstance(true);
		return fragment;
	}

	private FragmentActivity mActivity;
	private View mLayout;
	private WebView mWebView;
	private ProgressBar mLoading;
	
	private String mVcode;
	private int mBcode;
	private int mCnum;

	private JavaScriptInterface mJavascriptInterface = new JavaScriptInterface(this);
	private BroadcastReceiver mReceiver = new TextSizeReceiver(this);
	private IntentFilter filter = new IntentFilter("net.joostory.holybible.action.TEXT_SIZE");
	
	@Override
	public void onResume() {
		super.onResume();
		mActivity.registerReceiver(mReceiver, filter);
	}
	
	@Override
	public void onStop() {
		super.onStop();
		try {
			mActivity.unregisterReceiver(mReceiver);
		} catch (Throwable t) {
			Crashlytics.logException(t);
		}
	}
	
	@SuppressLint("SetJavaScriptEnabled")
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		mActivity = getActivity();
		mVcode = getArguments().getString("vcode");
		mBcode = getArguments().getInt("bcode");
		mCnum = getArguments().getInt("cnum");
		
		mLayout = inflater.inflate(R.layout.verses_list, container, false);
		
		mLoading = (ProgressBar) mLayout.findViewById(R.id.loading);

		mWebView = (WebView) mLayout.findViewById(R.id.content);
		mWebView.getSettings().setJavaScriptEnabled(true);
		mWebView.addJavascriptInterface(mJavascriptInterface, "JSInterface");
		mWebView.setWebViewClient(new WebViewClient() {
			@Override
			public void onPageFinished(WebView view, String url) {
				view.loadUrl(String.format("javascript:init('%dpx')", PreferenceUtils.getInt(mActivity, "textSize", 14)));
				mWebView.setVisibility(View.VISIBLE);
				mLoading.setVisibility(View.GONE);
			}
		});
		
		mWebView.loadUrl("file:///android_asset/verses/verses_template.html");
		
		return mLayout;
	}
	
	
	class JavaScriptInterface {
		private VersesFragment fragment;

		public JavaScriptInterface(VersesFragment fragment) {
	        this.fragment = fragment;
	    }
		
		@JavascriptInterface
		public String getVersesData() {
			StringBuffer buf = new StringBuffer();
			List<Verse> list = VerseDAO.getInstance().getList(fragment.mActivity.getApplicationContext(), fragment.mVcode, fragment.mBcode, fragment.mCnum);
			for (Verse v : list) {
				buf.append("<li value='").append(v.getVnum()).append("'>").append(v.getContent()).append("</li>");
			}
			return buf.toString();
		}
	}
	
	class TextSizeReceiver extends BroadcastReceiver {

		private VersesFragment fragment;

		public TextSizeReceiver(VersesFragment fragment) {
			this.fragment = fragment;
		}

		@Override
		public void onReceive(Context context, Intent intent) {
            int textSize = intent.getIntExtra("textSize", VersesActivity.TEXT_SIZE_DEFAULT);
			fragment.mWebView.loadUrl(String.format("javascript:setFontSize('%dpx')", textSize));
		}
	}

}
