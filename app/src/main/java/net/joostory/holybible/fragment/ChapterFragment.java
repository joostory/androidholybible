package net.joostory.holybible.fragment;

import net.joostory.holybible.R;
import net.joostory.holybible.activity.VersesActivity;
import net.joostory.holybible.dao.BibleDAO;
import net.joostory.holybible.model.Bible;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

public class ChapterFragment extends Fragment {

	public static Fragment newInstance(String vcode, int bcode) {
		ChapterFragment fragment = new ChapterFragment();
		Bundle bundle = new Bundle();
		bundle.putString("vcode", vcode);
		bundle.putInt("bcode", bcode);
		fragment.setArguments(bundle);
		return fragment;
	}

	private Bible bible;
	private View layout;
	
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		layout = inflater.inflate(R.layout.list, container, false);
		String vcode = getArguments().getString("vcode");
		int bcode = getArguments().getInt("bcode");
		
		init(vcode, bcode);
		
		return layout;
	}		
	
	
	private void init(String vcode, int bcode) {
		bible = BibleDAO.getInstance().get(getActivity().getApplicationContext(), vcode, bcode);
		
		ListView view = (ListView) layout.findViewById(R.id.list);
		view.setAdapter(new ChapterAdapter(bible));
        view.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> arg0, View v, int position, long id) {
				long itemId = arg0.getItemIdAtPosition(position);
				int cnum = (int) (itemId + 1);
				
				Intent intent = new Intent(getActivity(), VersesActivity.class);
				intent.putExtra("vcode", bible.getVcode());
				intent.putExtra("bcode", bible.getCode());
				intent.putExtra("cnum", cnum);
				startActivity(intent);
			}
		});
	}


	public class ChapterAdapter extends BaseAdapter implements ListAdapter {

    	Bible bible = null;

		public ChapterAdapter(Bible bible) {
			super();
			this.bible = bible;
		}


		@Override
		public int getCount() {
			return bible.getChapterCount();
		}

		@Override
		public String getItem(int position) {
			return bible.getName() + " " + (position + 1);
		}

		@Override
		public long getItemId(int position) {
			return position;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			String name = getItem(position);
			if (convertView == null) {
				LayoutInflater inflater = getLayoutInflater(null);
				convertView = inflater.inflate(R.layout.list_row, null, false);
			}
			TextView nameView = (TextView) convertView.findViewById(R.id.name);
			nameView.setText(name);
			
			return convertView;
		}
    }
}
