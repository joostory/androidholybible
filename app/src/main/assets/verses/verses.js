function setFontSize(fontSize) {
	var list = document.getElementById("list");
	list.style.fontSize = fontSize;
}

function init(fontSize) {
	var list = document.getElementById("list");
	list.innerHTML = window.JSInterface.getVersesData();
	setFontSize(fontSize);
}
