CREATE INDEX IF NOT EXISTS "bibles_index" on bibles (vcode ASC);
CREATE INDEX IF NOT EXISTS "verses_index" on verses (vcode ASC, bcode ASC, cnum ASC);